Crime Prediction in San Francisco based on Incidents and Temporal Schemes 
====================

![SFSyline](http://www.sanfrancisco.travel/sites/sftraveldev.prod.acquia-sites.com/files/SanFrancisco_0.jpg)

Criminality is a permanent problem that claims attention of security forces and governments. It is expected that it can be fought more effectively if information recorded previously about this kind of acts is analyzed in order to extract knowledge. This paper presents an approach concerning this topic applying machine learning techniques.  It is pretended to process [datasets of crimes committed in the city of San Francisco][dataset-crimes] through unsupervised and supervised techniques, so that it is possible to analyze if a relation between crimes and incidents exists and to make prognostics regarding the kind of crime that could be committed depending on temporal and spatial factors, studying the impact of temporal schemes on the prediction. 

To solve this the system has splited in three modules, pronspection, preprocessing and Data Mining:

* [Pronspection](#markdown-header-pronspection)
    * [get_data](#markdown-header-get_data)
    * [limpiar_incidentes](#markdown-header-limpiar_incidentes)
    * [agrupa_mes_incidente_crimen](#markdown-header-agrupa_mes_incidente_crimen)
    * [agrupa_semana_incidente_crimen](#markdown-header-agrupa_semana_incidente_crimen)
    * [filtra_fecha_incidente](#markdown-header-filtra_fecha_incidente)
    * [agrupa_puntos_crimenes_incidentes_tabla](#markdown-header-agrupa_puntos_crimenes_incidentes_tabla)
    * [grafica_agrupacion](#markdown-header-grafica_agrupacion)
    * [dendograma_correlacion](#markdown-header-dendograma_correlacion)
    * [agrupa_puntos_crimenes_incidentes](#markdown-header-agrupa_puntos_crimenes_incidentes)
    * [imagenes_evolucion](#markdown-header-imagenes_evolucion)
    * [crea_tablas](#markdown-header-crea_tablas)
    * [crea_tablas_2](#markdown-header-crea_tablas_2)
* [Preprocessing](#markdown-header-preprocessing)
    * [get_data](#markdown-header-get_data)
    * [add_distrito](#markdown-header-add_distrito)
    * [estandariza_tabla_crimenes](#markdown-header-estandariza_tabla_crimenes)
    * [make_sql_file_crimenes](#markdown-header-make_sql_file_crimenes)
* [Data Mining](#markdown-header-data-mining)
    * [ClusterBase](#markdown-header-clusterbase)
        * [ClusterHora](#markdown-header-clusterhora)
        * [ClusterDiaSemana](#markdown-header-clusterdiasemana)
        * [ClusterMes](#markdown-header-clustermes)
        * [ClusterTabla](#markdown-header-clustertabla)
            * [ClusterDistrito](#markdown-header-clusterdistrito)
            * [ClusterCrimen](#markdown-header-clustercrimen)
    * [read_crimenes_preprocesados](#markdown-header-read_crimenes_preprocesados)
    * [crear_datos_entrenamiento](#markdown-header-crear_datos_entrenamiento)
    * [entrenar_modelo](#markdown-header-entrenar_modelo)
    * [devolver_probabilidad_acumulada](#markdown-header-devolver_probabilidad_acumulada)
    * [cluster_probabilidad](#markdown-header-cluster_probabilidad)
    * [main](#markdown-header-main)

- - -

# ==> Pronspection

This is the first step of the work, we analize the dataset. The most important functions are as follows:

## get_data
Read a CSV file specified in the function, and iterate over each row of this. The function have four optionals
parameters:

* delimiter: the token that mark the separation of two or more columns. The default value is ','.
* quotechar: the token that mark the start and the end of a entire text, ignoring the delimiter token. The default value is '"'.
* titulo: indicate if the function need return the first row of the file. The default value is False.
* fl: a list of functions to convert the data of CSV file to this type (float, int,..). The function read the functions of the list until a function down raise a error, if none of the functions return a valid result, the function return the data has a string. The default value is a empty list.

## limpiar_incidentes

This function gets the data readed in [get_data](#markdown-header-get_data), and split the coordinates in two different variables, stripping also the white spaces.

## agrupa_mes_incidente_crimen

It gets the data of crimes and incidents and groups all the crimes that have befallen in the same month and year, returning for each combination of this two values, the elements belonging to that group.

## agrupa_semana_incidente_crimen

The same as above but returns for each week of each year the crimes and incidents that have occur that week.

## filtra_fecha_incidente

Given the data, a year and a month, it returns the events that have happened in this date.


## agrupa_puntos_crimenes_incidentes_tabla

The following method gets the following attributes:

* datos_crimenes: The data of the crimes
* datos_incidentes: The data of the incidents
* eps = An epsilon that represent the area that the cluster will use as maximum for the cluster.
* min_samples: The minimun amount of samples that a cluster have to have
*m: The map of San Francisco

It generates a table with the density cluster generated using the parameter information.

## dendograma_correlacion

It genereates an image with a dendogram showing the relations between the diferent types of crimes and incidents. It uses a hirerarchical clustering to do that.

## agrupa_puntos_crimenes_incidentes

This function calculate a density cluster with the crimes and incidents of San Francisco. It has the following attributes:

* datos_crimenes: The data of the crimes
* datos_incidentes: The data of the incidents
* eps = An epsilon that represent the area that the cluster will use as maximum for the cluster.
* min_samples: The minimun amount of samples that a cluster have to have
*m: The map of San Francisco

As a result, this method will display an image showing the position of the cluster in a map of San Francisco.

## imagenes_evolucion

It gets a set of data pretreated with the clusters already generated throw [crea_tablas](#markdown-header-crea_tablas), and create for each month of the data an image showing the clusters that have been made in that month.

## crea_tablas

Using [agrupa_puntos_crimenes_incidentes_tabla](#markdown-header-agrupa_puntos_crimenes_incidentes_tabla), it generates a csv with the cluster information for each month of each year.

## crea_tablas_2

Using [agrupa_puntos_crimenes_incidentes_tabla](#markdown-header-agrupa_puntos_crimenes_incidentes_tabla), it generates a csv with the cluster information for each week of each year.


- - -

# ==> Preprocessing

## get_data

The same function that is shown [above](#markdown-header-get_data).

## add_distrito
Take a iterator of the dataset of crimes of SF and return a iterator with the previous data plus the neighborhood of
each entry.

To do this we ask to a geographical database if a point is inside a neighborhood, previously sorted by the size of the
neighborhood, and the system put a crime in the first neighborhood that wraps the crime position.

As some are false crime positions, we dismiss these entries. The same procedure applies to the entries outside one
neighborhood of SF.


## estandariza_tabla_crimenes
Preprocess a iterator of crimes in SF. The main goals are:

* Join the date and the time in a single field
* Put all the strings in lowercase
* Remove innecessary fields

The functions return a iterator with this columns:

* Type of crime
* Description of the crime
* Day of week
* Date and hour
* Neighborhood
* Longitude
* Latitude

## make_sql_file_crimenes
Make a SQLite database with a preprocess iterator. 

The fields and types of the SQLite database are:

Name        | Type of data
------------|---------------
Categoria   | TEXT
Descripcion | TEXT
DiaSemana   | TEXT
Fecha       | TIMESTAMP
Distrito    | TEXT
X           | REAL
Y           | REAL

- - -

# ==> Data Mining

## ClusterBase
Interface to encapsulate the behaviour of diferents classes of prediction system. The interface functions are:

* cluster( item ): return the cluster id of one item
* name( cluster ): return a 'Human-readable' label of the cluster
* len_cluster(): return the number of classes in the cluster

Of this interface inherits the following classes:
### ClusterHora
Return a integer according to the time slot of a date

### ClusterDiaSemana
Return a integer according to the day of week

### ClusterMes
Return a integer according to the month of the year

### ClusterTabla
This class have a extra function of training that take a table of crimes and define a numerical index to the values do not repeated,
this is the numerical id of a item.

Two classes take this as a base class:

+ **ClusterDistrito**
  -Take the column of neighborhood to the training

+ **ClusterCrimen**
  -Take the column of type of crime to the training

## read_crimenes_preprocesados
Read the SQLite database created in the preprocessing step. Depending of his arguments the function filters some data:

* If the funtion have two arguments read all the crimes between two dates, the first argument is the start date
  (inclusive), and the second is the last date (exclusived)
* If the function dont have arguments read all the crimes

## crear_datos_entrenamiento
Return two list treatable by the model. The first with the input of the model (hour intervals, day of week and neighborhood) and the second with the type of crime.

Take the function as input:

+ An iterator of crimes

+ A list of tuples with the number of the column and the cluster
instance by the model input

+ A tuple with the number of column ad the cluster as result of the model.

## entrenar_modelo
Function that train a Naive-Bayes classifier with crimes entries ocurred between two dates taken as arguments.

The function can be divided into three parts. The first train the instances inherit from 'ClusterTabla' with the entire
database, the second produce the input necessary to train the Naive-Bayes classifier, and the last one train the Naive-Bayes
classifier with the generated input.

## devolver_probabilidad_acumulada
Return the upper bound of a probability sector passed as an argument

## cluster_probabilidad
Return given a accumulated probability a numerical index that repressent his sector

The default sectors in the system are:

* 0% ~ 50%
* 50% ~ 75%
* 75% ~ 90%
* 90% ~ 100%

## main
The rest of code use these functions to train a Multinomial Naive-Bayes model, and do a validation
of the model with an extra month on three neighborhood ('Mission', 'South of Market' and 'Tenderloin').

The results of the validations are repressent as heatmap, where a vivid color represent a better prediction and the
number inside each heatmap square represent the number of crimes produced each date.

[dataset-crimes]: https://data.sfgov.org/Public-Safety/Map-Crime-Incidents-from-1-Jan-2003/gxxq-x39z