#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sqlite3 as lite
from datetime import datetime
from itertools import groupby

#http://docs.scipy.org/doc/scipy/reference/cluster.html
from sklearn.naive_bayes import GaussianNB,MultinomialNB
import numpy as np

import matplotlib.pyplot as plt

def read_crimenes_preprocesados( *args ):
    connection = lite.connect('./data/out/crimenes_preprocesados.db')

    if len( args ) == 2:
        consulta_sql = [
                "SELECT c.* FROM Crimenes c WHERE ( c.Fecha BETWEEN ? AND ? )",
                args,
                ]
    else:
        consulta_sql = [
                "SELECT c.* FROM Crimenes c"
                ]

    with connection:
        cursor = connection.cursor()
        resultado_consulta = cursor.execute( *consulta_sql )
        for fila in resultado_consulta:
            l = []
            for v in fila:
                l.append( v )

            d = datetime.strptime( l[ 3 ], "%Y-%m-%d %H:%M:%S" )
            l[ 3 ] = d

            yield l

class ClusterBase(object):
    def cluster( self, item ):
        raise NotImplementedError

    def name( self, cluster ):
        raise NotImplementedError

    def len_cluster( self ):
        raise NotImplementedError

class ClusterHora(ClusterBase):
    def __init__( self ):
        self._horas = [
            (0,45),
            (6,10),
            (10,45),
            (14,15),
            (16,30),
            (18,20),
            (22,20)
        ]

    def cluster( self, fecha ):
        t1 = ( fecha.hour, fecha.minute )
        for i, t2 in enumerate( self._horas ):
            if t1 < t2:
                return i

        return 0

    def name( self, cluster ):
        return ( 
                self._horas[ (cluster-1) % len( self._horas ) ], 
                self._horas[ cluster ]
        )

    def __repr__( self ):
        return 'Franja horaria'

    def len_cluster( self ):
        return len( self._horas )

class ClusterDiaSemana(ClusterBase):
    def __init__( self ):
        self._lista_dias = [
            'monday',
            'tuesday',
            'wednesday',
            'thursday',
            'friday',
            'saturday',
            'sunday'
        ]

        self._dic_dias = {}
        for i, dia in enumerate( self._lista_dias ):
            self._dic_dias[ dia ] = i

    def cluster( self, dia_semana ):
        return self._dic_dias[ dia_semana ]

    def name( self, cluster ):
        return self._lista_dias[ cluster ]

    def __repr__( self ):
        return 'Dia de la semana'

    def len_cluster( self ):
        return len( self._lista_dias )

class ClusterMes(ClusterBase):
    def __init__( self ):
        self._lista_meses = [
            'january',
            'february',
            'march',
            'april',
            'may',
            'june',
            'july',
            'august',
            'september',
            'october',
            'november',
            'december'
        ]

        self._dic_meses = {}
        for i, mes in enumerate( self._lista_meses ):
            self._dic_meses[ mes ] = i

    def cluster( self, fecha ):
        return fecha.month-1

    def name( self, cluster ):
        return self._lista_meses[ cluster ]

    def __repr__( self ):
        return 'Mes'

    def len_cluster( self ):
        return len( self._lista_meses )

class ClusterTabla(ClusterBase):
    def __init__( self, f_obtener_item ):
        self._f_obtener_item = f_obtener_item
        self._dic_items = {}
        self._list_items = []

    def train( self, table ):
        items = set( map( self._f_obtener_item, table ) )

        self._dic_items = {}
        self._list_items = []
        for id_item, item in enumerate( items ):
            self._dic_items[ item ] = id_item
            self._list_items.append( item )

    def cluster( self, item ):
        return self._dic_items[ item ]

    def name( self, cluster ):
        return self._list_items[ cluster ]

    def len_cluster( self ):
        return len( self._list_items )

class ClusterDistrito(ClusterTabla):
    def __init__( self ):
        ClusterTabla.__init__( self, lambda x: x[4] )

    def __repr__( self ):
        return 'Distrito'

class ClusterCrimen(ClusterTabla):
    def __init__( self ):
        ClusterTabla.__init__( self, lambda x: x[0] )

    def __repr__( self ):
        return 'Crimen'

def crear_datos_entrenamiento( tabla, clusters_entrenamiento, cluster_crimes ):
    datos_entrada = []
    datos_salida = []
    for fila in tabla:
        # hour intervals | day of week | neighborhood
        entrada = []
        for idx_fila, cluster in clusters_entrenamiento:
            entrada.append( cluster.cluster( fila[ idx_fila ] ) )
        datos_entrada.append( tuple( entrada ) )

        datos_salida.append(
                cluster_crimes[1].cluster( fila[ cluster_crimes[0] ] )
        )

    return ( datos_entrada, datos_salida )

def entrenar_modelo( modelo_naive_bayes, de, a):
    # first we obtain the cluster values
    cNeighborshoods = ClusterDistrito()
    cCrimes = ClusterCrimen()
    for c in [cNeighborshoods, cCrimes]:
        c.train( read_crimenes_preprocesados() )

    cHora = ClusterHora()
    cDia_semana = ClusterDiaSemana()
    cMes = ClusterMes()

    clusters_entrenamiento = [
        ( 4, cNeighborshoods ),
        ( 3, cHora ),
        ( 2, cDia_semana ),
        ( 3, cMes )
    ]

    cluster_crimes = ( 0, cCrimes )

    train_values, train_answers = crear_datos_entrenamiento(
            read_crimenes_preprocesados( de, a ),
            clusters_entrenamiento,
            cluster_crimes
    )

    # train the model using the training sets 
    modelNB.fit( train_values, train_answers )

    return ( clusters_entrenamiento, cluster_crimes )

def devolver_probabilidad_acumulada( grupo, grupos ):
    if grupo < len( grupos ):
        return grupos[ grupo ]
    else:
        return 1.0

def cluster_probabilidad( probabilidad, grupos ):
    for i, p in enumerate( grupos ):
        if probabilidad < p:
            return i

    return len( grupos )

# gaussian classifier
modelNB = MultinomialNB()

#de_entrenamiento = datetime( 2007,1,1 )
de_entrenamiento = datetime( 2004,1,1 )
a_entrenamiento = datetime( 2012,1,1 )

cInput, cAnswer = entrenar_modelo( modelNB, de_entrenamiento, a_entrenamiento )

# test the model

de_pruebas = datetime( 2012,1,1 )
a_pruebas = datetime( 2015,1,1 )
#a_pruebas = datetime( 2013,1,1 )

GENERAR_IMAGENES = True

for distrito in [ 'mission', 'south of market', 'tenderloin' ]:
    lista_datos = list( filter( lambda x: x[4] == distrito, read_crimenes_preprocesados( de_pruebas, a_pruebas ) ) )

    data_test_input, data_test_answers = crear_datos_entrenamiento(
        lista_datos,
        cInput,
        cAnswer
    )

    predicted_probs = modelNB.predict_proba(data_test_input)

    # print( len( predicted_probs[0] ) )
    # print( len( cAnswer[1]._dic_items ) )

    valor_medio_puesto_validacion_1 = 0
    valor_medio_puesto_validacion_2 = 0
    cRespuesta = cAnswer[1]
    respuestas_pos_validacion_1 = []
    respuestas_prob_validacion_1 = []
    respuestas_pos_validacion_2 = []
    respuestas_prob_validacion_2 = []

    grupos_probabilidades_acumuladas = [ 0.5, 0.75, 0.9 ]

    matriz_media_crimenes = []
    matriz_numero_crimenes = []
    dias = []

    VERBOSE = False

    for a, b in \
        groupby(
            sorted(
                zip(
                    map(
                        lambda x: x[3].strftime("%y/%m/%d"),
                        lista_datos
                    ),
                    enumerate( zip( predicted_probs, data_test_input, data_test_answers ) )
                ),
                key = lambda x: x[0]
            ),
            key = lambda x: x[0]
        ):

        dias.append( a )

        fila_crimenes = [ 0.0 ] * cRespuesta.len_cluster()
        numero_crimenes = [ 0 ] * cRespuesta.len_cluster()

        if VERBOSE:
            print( '#' * 80 )
            print( 'Analizando el día: %s' % ( a ) )
            print( '#' * 80 )

        for _, ( i_probs, ( probs, entrada, respuesta_real ) ) in b:
            if VERBOSE:
                print( 'Caso de test %d' % ( i_probs ) )
                print( 'Entradas al sistema:' )
                for c, v in zip( cInput, entrada ):
                    print( '  - %s: %s' % ( c[1], c[1].name( v ) ) )
                print( 'Resultado real: %s' % ( cRespuesta.name( respuesta_real ) ) )
                print()
                print( 'Probabilidades segun el modelo:' )
            #for puesto, (i, prob) in enumerate( reversed( sorted( enumerate( probs ), key = lambda x: x[1] ) ) ):
                #print( '  - ( %.2f ) %s' % ( prob, cRespuesta.name( i ) ) )
                # if i == respuesta_real:
                    # valor_medio_puesto += puesto

            ###################################
            # probabilidad igual
            ###################################

            for pos, ( valor, probabilidades_agrupadas ) in enumerate( 
                    groupby(
                        reversed( sorted( enumerate( probs ), key = lambda x: x[1] ) ),
                        key = lambda x: int( x[1] * 100 ) 
                    )
                ):
    
                if VERBOSE:
                    print( '  - Agrupacion %d: ( %.2f )' % ( pos, valor / 100.0 ) )
                    for i, prob in probabilidades_agrupadas:
                        print( '    - ( %.2f ) %s' % ( prob, cRespuesta.name( i ) ) )

                for i, prob in probabilidades_agrupadas:
                    if i == respuesta_real:
                        valor_medio_puesto_validacion_1 += pos
                        respuestas_prob_validacion_1.append( prob )
                        respuestas_pos_validacion_1.append( pos )
                        break



            ###################################
            # probabilidad acumulada
            ###################################

            probabilidades_acumuladas = []
            p_acumulada = 0.0
            for pos, probabilidad in reversed( sorted( enumerate( probs ), key = lambda x: x[1] ) ):
                p_acumulada += probabilidad
                probabilidades_acumuladas.append( ( pos, probabilidad, p_acumulada ) )

            for pos, ( valor, probabilidades_agrupadas ) in enumerate( 
                    groupby(
                        probabilidades_acumuladas,
                        key = lambda x: cluster_probabilidad( x[2], grupos_probabilidades_acumuladas )
                    )
                ):

                if VERBOSE:
                    print( '  - Agrupacion %d: ( %.2f )' % ( 
                        pos, 
                        devolver_probabilidad_acumulada( valor, grupos_probabilidades_acumuladas )
                    ) )
                for i, prob, prob_acumulada in probabilidades_agrupadas:
                    if VERBOSE:
                        print( '    - ( %.2f - %.2f ) %s' % ( prob_acumulada, prob, cRespuesta.name( i ) ) )

                    if i == respuesta_real:
                        valor_medio_puesto_validacion_2 += pos
                        respuestas_prob_validacion_2.append( prob )
                        respuestas_pos_validacion_2.append( pos )

                        fila_crimenes[ i ] += len( grupos_probabilidades_acumuladas ) - pos
                        numero_crimenes[ i ] += 1
                        if not VERBOSE:
                            break

            if VERBOSE:
                print()
                print( '-' * 80 )
                print()

        for i in range( cRespuesta.len_cluster() ):
            if numero_crimenes[ i ] != 0:
                fila_crimenes[ i ] /= float( numero_crimenes[ i ] )

        matriz_media_crimenes.append( fila_crimenes )
        matriz_numero_crimenes.append( numero_crimenes )

    print( 'Distrito: %s' % ( distrito ) )
    valor_medio_puesto_validacion_1 /= float( len( data_test_input ) )
    print( 
            'Media del puesto real de la respuesta segun el clasificador (validación 1): %.2f' % ( 
                valor_medio_puesto_validacion_1
                ) 
    )

    valor_medio_puesto_validacion_2 /= float( len( data_test_input ) )
    print( 
            'Media del puesto real de la respuesta segun el clasificador (validación 2): %.2f' % ( 
                valor_medio_puesto_validacion_2
                ) 
    )

    respuestas_prob_validacion_1.sort()
    respuestas_pos_validacion_1.sort()
    mediana_pos = respuestas_pos_validacion_1[ len( respuestas_pos_validacion_1 ) // 2 ]
    mediana_prob = respuestas_prob_validacion_1[ len( respuestas_prob_validacion_1 ) // 2 ]
    print( 'Mediana del grupo de pertenencia: %d' % ( mediana_pos ) )
    print( 'Mediana de la probabilidad: %.2f' % ( mediana_prob ) )

    print( 'Numero de elementos por grupo' )
    for v, elementos in groupby( respuestas_pos_validacion_1 ):
        n_elementos = sum( 1 for _ in elementos )
        print( 'Grupo %d: %d/%d (%.2f%%)' % ( 
            v, n_elementos, len( respuestas_pos_validacion_1 ), 
            100.0 * n_elementos / float( len( respuestas_pos_validacion_1 ) ) 
        ) )

    print( '#' * 80 )

    respuestas_prob_validacion_2.sort()
    respuestas_pos_validacion_2.sort()
    mediana_pos = respuestas_pos_validacion_2[ len( respuestas_pos_validacion_2 ) // 2 ]
    mediana_prob = respuestas_prob_validacion_2[ len( respuestas_prob_validacion_2 ) // 2 ]
    print( 'Mediana del grupo de pertenencia: %d' % ( mediana_pos ) )
    print( 'Mediana de la probabilidad: %.2f' % ( mediana_prob ) )

    print( 'Numero de elementos por grupo' )
    for v, elementos in groupby( respuestas_pos_validacion_2 ):
        n_elementos = sum( 1 for _ in elementos )
        print( 'Grupo %d: %d/%d (%.2f%%)' % ( 
            v, n_elementos, len( respuestas_pos_validacion_2 ), 
            100.0 * n_elementos / float( len( respuestas_pos_validacion_2 ) ) 
        ) )


    if GENERAR_IMAGENES:
        #plt.figure(figsize=(10, 100), dpi=60)
        plt.figure(figsize=(10, 100), dpi=60)

        plt.subplot( 111 )
        plt.imshow( matriz_media_crimenes, cmap=plt.cm.hot, interpolation='nearest')
        plt.xticks(
                range( cRespuesta.len_cluster() ), 
                list( map( lambda i: cRespuesta.name( i ).capitalize(), range( cRespuesta.len_cluster() ) ) ),
                rotation=90, 
                size=6
        )

        plt.yticks(
                range( len( matriz_media_crimenes ) ), 
                dias,
                size=6
        )

        for y in range( len( matriz_media_crimenes ) ):
            for x in range( cRespuesta.len_cluster() ):
                plt.text(
                        x, 
                        y,
                        '%d' % ( matriz_numero_crimenes[y][x] ),
                        horizontalalignment='center',
                        verticalalignment='center',
                        color='green',
                        size=6
                )

#plt.show()
        plt.savefig( './data/out/mapa_calor-entrenamiento(%s,%s)-validacion(%s,%s)-distrito(%s).png' %
                ( 
                    de_entrenamiento.strftime("%y-%m-%d"),
                    a_entrenamiento.strftime("%y-%m-%d"),
                    de_pruebas.strftime("%y-%m-%d"),
                    a_pruebas.strftime("%y-%m-%d"),
                    distrito
                ) 
        )

    print( '-' * 80 )
    print()
