#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import sys
import matplotlib.image as mpimg

class SFMapBase(object):
    def __init__(self, n ):
        self._img = mpimg.imread( n )

    def get_map( self ):
        return self._img

    def _get_pxls_values( self ):
        raise NotImplementedError

    def get_pxl( self, lon, lat ):
        ca, pa, cb, pb = self._get_pxls_values()
        va = pa[0] + (pb[0]-pa[0])*(lon - ca[0])/(cb[0] - ca[0])
        vb = pa[1] + (pb[1]-pa[1])*(lat - ca[1])/(cb[1] - ca[1])

        if not( 0 <= va and va <= len( self._img[0] ) and \
                0 <= vb and vb <= len( self._img  ) ):
            return None

        return ( va, vb )


class SFMap(SFMapBase):
    def __init__(self):
        SFMapBase.__init__( self, './data/in/maps/sf-claro.png' )

    def _get_pxls_values( self ):
        ca = ( -122.483552, 37.722858 )
        pa = ( 533, 1512 )

        cb = ( -122.424509, 37.810584 )
        pb = ( 1266, 134 )

        return ( ca, pa, cb, pb )

class SFLargeMap(SFMapBase):
    def __init__(self):
        SFMapBase.__init__( self, './data/in/maps/sf2-claro.png' )

    def _get_pxls_values( self ):
        ca = ( -122.483552, 37.722858 )
        pa = ( 2324, 6132 )

        cb = ( -122.424509, 37.810584 )
        pb = ( 4775, 1529 )

        return ( ca, pa, cb, pb )


if __name__ == "__main__":
    lon = float(sys.argv[1])
    lat = float(sys.argv[2])

    m = SFMap()

    print( m.get_pxl( lon, lat ) )
