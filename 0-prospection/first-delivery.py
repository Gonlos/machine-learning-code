#!/usr/bin/env python
# -*- coding: utf-8 -*-
import csv
from itertools import groupby, combinations, permutations
import numpy as np
import matplotlib.pyplot as plt
import sqlite3 as lite
from sklearn import preprocessing 
import os
from sfmap import SFMap, SFLargeMap
import colorsys
import random
import re
from nltk.stem import WordNetLemmatizer
from nltk.stem.porter import PorterStemmer
from nltk.corpus import stopwords
import nltk
from nltk.corpus import wordnet as wn
import sys
from sklearn import preprocessing 
import math

#http://docs.scipy.org/doc/scipy/reference/cluster.html
from scipy import cluster
import scipy

# http://scikit-learn.org/stable/modules/preprocessing.html
from sklearn import preprocessing
from scipy.spatial.distance import cdist

import sklearn.neighbors

from sklearn.preprocessing import StandardScaler

from sklearn.cluster import DBSCAN

from sklearn import metrics
from sklearn.datasets.samples_generator import make_blobs
from sklearn.preprocessing import StandardScaler

from sklearn.metrics import pairwise_distances
from scipy.spatial.distance import cosine
import datetime

from os import listdir

def get_random_color(pastel_factor = 0.5):
    return [(x+pastel_factor)/(1.0+pastel_factor) for x in [random.uniform(0,1.0) for i in [1,2,3]]]

def color_distance(c1,c2):
    return sum([abs(x[0]-x[1]) for x in zip(c1,c2)])

def generate_new_color(existing_colors,pastel_factor = 0.5):
    max_distance = None
    best_color = None
    for i in range(0,100):
        color = get_random_color(pastel_factor = pastel_factor)
        if not existing_colors:
            return color
        best_distance = min([color_distance(color,c) for c in existing_colors])
        if not max_distance or best_distance > max_distance:
            max_distance = best_distance
            best_color = color
    return best_color

def read_crimenes( barrio ):
    connection = lite.connect('./data/sql/crimenes_distr.db')

    consulta_sql ="SELECT c.* FROM crim c, neigh1 n WHERE c.IncidntNum = n.IncidntNum AND n.PdDistrict = '{}'".format( barrio )
    with connection:
        cursor = connection.cursor()
        resultado_consulta = cursor.execute(consulta_sql)
        for fila in resultado_consulta:
            l = []
            for v in fila:
                l.append( v )

            yield l

def read_incidentes( barrio ):
    connection = lite.connect('./data/sql/incidentes_distr.db')

    consulta_sql ="SELECT i.* FROM incid i, neigh1 n WHERE i.CaseID = n.CaseID AND n.Neighborhood = '{}'".format( barrio )
    with connection:
        cursor = connection.cursor()
        resultado_consulta = cursor.execute(consulta_sql)
        for fila in resultado_consulta:
            l = []
            for v in fila:
                l.append( v )

            yield l

def get_data( file_name, delimiter = ',', quotechar = '"', titulo=False, fl = [] ):
    data = None
    title = []
    with open( file_name ) as csvfile:
        reader = csv.reader(csvfile, delimiter=delimiter, quotechar=quotechar)
        for row in reader:
            if data == None:
                data = []
                for x in row:
                    title.append( x )
                if titulo:
                    yield tuple( title )
            else:
                l = []
                for x in row:
                    for f in fl + [ lambda x:x ]:
                        try:
                            l.append( f( x ) )
                            break
                        except:
                            pass
                #data.append( l )
                yield tuple( l )

    #return ( title, data )

def limpiar_incidentes( data ):
   for CaseID,Opened,Closed,Updated,Status,ResponsibleAgency,Category,\
               RequestType,RequestDetails,Address,SupervisorDistrict,\
               Neighborhood,Point,Source,MediaURL in data:
        v = Point[1:-1].split(',')
        x = v[1].strip()
        y = v[0].strip()

        yield ( CaseID,Opened,Closed,Updated,Status,ResponsibleAgency,Category,\
               RequestType,RequestDetails,Address,SupervisorDistrict,\
               Neighborhood,x,y,Source )

def agrupa_mes_incidente_crimen( datos_incidentes, datos_crimenes ):
    dic_agrupaciones = {}

    for v in datos_incidentes:
        m, d, y = tuple( map( int, v[1].split( ' ' )[0].split( '/' ) ) )

        _,_,_,_,_,_,categoria_incidentes,_,_,_,_,_,puntos_x,puntos_y,_ = v
        v = None,None,None,None,None,None,categoria_incidentes,None,None,None,None,None,puntos_x,puntos_y,None

        key = (y, m)
        try:
            dic_agrupaciones[ key ][0].append( v )
        except:
            dic_agrupaciones[ key ] = [ [ v ], [] ]

    for v in datos_crimenes:
        m, d, y = tuple( map( int, v[4].split( ' ' )[0].split( '/' ) ) )

        #_,categoria_crimenes,_,_,_,_,_,_,_,puntos_x,puntos_y,_ = v # normal
        _,categoria_crimenes,_,_,_,_,_,_,_,puntos_x,puntos_y = v # sql
        v = None,categoria_crimenes,None,None,None,None,None,None,None,puntos_x,puntos_y,None

        key = (y, m)
        try:
            dic_agrupaciones[ key ][1].append( v )
        except:
            dic_agrupaciones[ key ] = [ [], [ v ] ]

    for fecha, valores in dic_agrupaciones.items():
        yield fecha, valores[0], valores[1]



def agrupa_semana_incidente_crimen( datos_incidentes, datos_crimenes ):
    dic_agrupaciones = {}

    for v in datos_incidentes:
        m, d, y = tuple( map( int, v[1].split( ' ' )[0].split( '/' ) ) )

        #print( d, m, y )

        numero_semana = datetime.date(y, m, d).isocalendar()[1]

        _,_,_,_,_,_,categoria_incidentes,_,_,_,_,_,puntos_x,puntos_y,_ = v
        v = None,None,None,None,None,None,categoria_incidentes,None,None,None,None,None,puntos_x,puntos_y,None

        key = (y, numero_semana)
        try:
            dic_agrupaciones[ key ][0].append( v )
        except:
            dic_agrupaciones[ key ] = [ [ v ], [] ]

    for v in datos_crimenes:
        m, d, y = tuple( map( int, v[4].split( ' ' )[0].split( '/' ) ) )

        numero_semana = datetime.date(y, m, d).isocalendar()[1]

        #_,categoria_crimenes,_,_,_,_,_,_,_,puntos_x,puntos_y,_ = v # csv
        _,categoria_crimenes,_,_,_,_,_,_,_,puntos_x,puntos_y = v # sql
        v = None,categoria_crimenes,None,None,None,None,None,None,None,puntos_x,puntos_y,None

        key = (y, numero_semana)
        try:
            dic_agrupaciones[ key ][1].append( v )
        except:
            dic_agrupaciones[ key ] = [ [], [ v ] ]

    for fecha, valores in dic_agrupaciones.items():
        yield fecha, valores[0], valores[1]

def filtra_fecha_incidente( data, year = None, month = None ):
    for v in data:
        m, d, y = tuple( map( int, v[1].split( ' ' )[0].split( '/' ) ) )
        if year != None and y != year: continue
        if month != None and m != month: continue

        _,_,_,_,_,_,categoria_incidentes,_,_,_,_,_,puntos_x,puntos_y,_ = v
        v = None,None,None,None,None,None,categoria_incidentes,None,None,None,None,None,puntos_x,puntos_y,None

        yield v

def filtra_fecha_crimen( data, year = None, month = None ):
    for v in data:
        m, d, y = tuple( map( int, v[4].split( ' ' )[0].split( '/' ) ) )
        if year != None and y != year: continue
        if month != None and m != month: continue

        _,categoria_crimenes,_,_,_,_,_,_,_,puntos_x,puntos_y,_ = v
        v = None,categoria_crimenes,None,None,None,None,None,None,None,puntos_x,puntos_y,None

        yield v

def agrupa_puntos_crimenes_incidentes_tabla( datos_crimenes, datos_incidentes, eps = 0.0006, min_samples = 5 ):
    m = SFMap()
    #m = SFLargeMap()

    _,categoria_crimenes,_,_,_,_,_,_,_,puntos_x,puntos_y,_ = zip( *datos_crimenes )
    puntos_x = map( float, puntos_x )
    puntos_y = map( float, puntos_y )

    puntos_filtrados_1 = list( filter( lambda v: m.get_pxl( *v[:-1] ), zip( puntos_x, puntos_y, categoria_crimenes ) ) )


    _,_,_,_,_,_,categoria_incidentes,_,_,_,_,_,puntos_x,puntos_y,_ = zip( *datos_incidentes )
    puntos_x = map( float, puntos_x )
    puntos_y = map( float, puntos_y )

    puntos_filtrados_2 = list( filter( lambda v: m.get_pxl( *v[:-1] ), zip( puntos_x, puntos_y, categoria_incidentes ) ) )

    puntos_filtrados = []
    puntos_filtrados.extend( puntos_filtrados_1 )
    puntos_filtrados.extend( puntos_filtrados_2 )

    px, py, categorias_puntos = zip( *puntos_filtrados )
    puntos_filtrados = list( zip( px, py ) )

    #matriz_distancias = scipy.spatial.distance.pdist( puntos_filtrados, metric='cityblock' )

    dist = sklearn.neighbors.DistanceMetric.get_metric('euclidean')
    matsim = dist.pairwise(puntos_filtrados)
    avSim = np.average(matsim)
    
    # 3. Building the Dendrogram	
    # http://docs.scipy.org/doc/scipy/reference/generated/scipy.cluster.hierarchy.linkage.html#scipy.cluster.hierarchy.linkage

    #puntos_filtrados = StandardScaler().fit_transform(puntos_filtrados)

    #labels = DBSCAN( metric='euclidean', eps = 0.0006, min_samples = 6 ).fit_predict( puntos_filtrados )
    labels = DBSCAN( metric='euclidean', eps = eps, min_samples = min_samples ).fit_predict( puntos_filtrados )


    # categoria = []

    dic_indice = {}
    lc = list( sorted( set( categoria_crimenes ) ) )
    li = list( sorted( set( categoria_incidentes ) ) )
    
    lci = []
    lci.extend( lc )
    lci.extend( li )

    for c, i in zip( lci, range( len( lci ) ) ):
        dic_indice[ c ] = i

    yield (('Centro Longitud', 'Centro Latitud', 'Numero elementos'), lci)

    for cluster, datos in groupby( sorted( zip( labels, categorias_puntos ) ), lambda v: v[0] ):
        if cluster == -1: continue
        _, categorias_cluster = zip( *datos )
        l = [ 0 ] * len( dic_indice )
        for categoria, elementos in groupby( sorted( categorias_cluster ) ):
            l[ dic_indice[ categoria ] ] = sum( 1 for _ in elementos )
        
        xy, _ = zip( *list( filter( lambda v: v[1] == cluster, zip( puntos_filtrados, labels ) ) ) )
        x, y = map( list, zip( *xy ) )

        cx = sum( x ) / len( x )
        cy = sum( y ) / len( y )

        yield ((cx, cy, sum( l )), l)

def grafica_agrupacion( tabla, categoria_a_ordenar ):
    indice_categoria = tabla[0].index( categoria_a_ordenar )

    valores = tuple( zip( *( tabla[1:] ) ) )[ indice_categoria ]
    
    _, indices = zip( *sorted( zip( valores, range( len( valores ) ) ) ) )

    tabla_ordenada_categoria = map( lambda v: tabla[ v+1 ], indices )
    valores_y = zip( *tabla_ordenada_categoria )

    valores_y_normalizados = list( valores_y )

    #valores_y_normalizados = preprocessing.normalize( list(valores_y) )

    colors = []
    for i in range(len(tabla[0])):
        colors.append(generate_new_color(colors, pastel_factor = 0.5) )

    for vy, categoria, color in zip( valores_y_normalizados, tabla[0], colors ):
        #if categoria != categoria_a_ordenar: continue
        v_min = min( vy )
        v_max = max( vy )
        v = list( map( lambda x: (x-v_min)/(v_max-v_min), vy ) )

        plt.plot( range( len( vy ) ), v, label = categoria, color = color )

    plt.legend(loc=5, borderaxespad=-20, prop={'size':8})
    plt.show()

def dendograma_correlacion( tabla, nombre = '' ):
    m = tabla[1:]
    #m = preprocessing.normalize( m ) 
    m = list(zip( *m ))
    m = preprocessing.normalize( m ) 
    m_similitud = np.corrcoef( m )
    m_similitud = np.nan_to_num( m_similitud )
    #m_similitud = abs( m_similitud )
            
    plt.figure(figsize=(20, 15), dpi=60)

    plt.subplot( 121 )
    plt.imshow( m_similitud, cmap=plt.cm.hot, interpolation='nearest')
    plt.xticks(range(len(tabla[0])), tabla[0], rotation=90, size=6)
    plt.yticks(range(len(tabla[0])), tabla[0], size=6)
    #plt.show()

    # vector_varianza = map( np.std, m )

    # print( sum( m_similitud[0] ) )
    # print( m_similitud[0] )

    # for v, c in sorted( zip( vector_varianza, tabla[0] ) ):
        # print( '{}: {}'.format( c, v ) )

    # #plt.figure(figsize=(35, 15), dpi=60)
    # plt.imshow( m_similitud, cmap=plt.cm.hot, interpolation='nearest')
    # plt.xticks(range(len(tabla[0])), tabla[ 0 ], rotation=90)
    # plt.yticks(range(len(tabla[0])), tabla[ 0 ])
    # plt.show()

    # #dist = sklearn.neighbors.DistanceMetric.get_metric('cosine')
    # m_similitud_coseno = pairwise_distances(m, metric="cosine")

    # #m_similitud = m_similitud_correlacion * m_similitud_coseno
    # m_similitud = m_similitud_correlacion * ( np.ones( ( len( m ), len( m[0] ) ) ) - m_similitud_coseno )

    # print( m_similitud_correlacion )
    # print( np.ones( ( len( m ), len( m[0] ) ) ) - m_similitud_coseno )
    # print( m_similitud )
    # # for i in range( len( m_similitud ) ):
        # # m_similitud[ i ][ i ] = 1

    # 3. Building the Dendrogram
    # http://docs.scipy.org/doc/scipy/reference/generated/scipy.cluster.hierarchy.linkage.html#scipy.cluster.hierarchy.linkage
    # plt.subplot( 111 )
    plt.subplot( 122 )

    clusters = cluster.hierarchy.linkage(m_similitud, method = 'ward')
    cluster.hierarchy.dendrogram(clusters, color_threshold=0, labels = tabla[0] )

    ax = plt.gca()
    xlbls = ax.get_xmajorticklabels()
    for lbl in xlbls:
        lbl.set_rotation( 90 )
        lbl.set_size( 6 )

    plt.savefig( './resultados/imagenes/dendograma_{}.png'.format( nombre ) )
    #plt.show()
    plt.close('all')

def estandariza_tabla( tabla, head_mod ):
    d = {}
    for k, v in zip( head_mod, range( len( head_mod ) ) ):
        d[ k ] = v

    head = tabla[0]
    nuevos_indices = list( map( lambda v: d[ v ], head ) )

    yield head_mod

    for fila in tabla[1:]:
        fila_mod = [ 0 ] * len( head_mod )
        for dato, indx in zip( fila, nuevos_indices ):
            fila_mod[ indx ] = dato
        yield fila_mod

def agrupa_puntos_crimenes_incidentes( datos_crimenes, datos_incidentes, eps = 0.0006, min_samples = 5 ):
    m = SFMap()
    #m = SFLargeMap()

    _,_,_,_,_,_,_,_,_,puntos_x,puntos_y,_ = zip( *datos_crimenes )
    puntos_x = map( float, puntos_x )
    puntos_y = map( float, puntos_y )

    puntos_filtrados_1 = list( filter( lambda v: m.get_pxl( *v ), zip( puntos_x, puntos_y ) ) )


    _,_,_,_,_,_,_,_,_,_,_,_,puntos_x,puntos_y,_ = zip( *datos_incidentes )
    puntos_x = map( float, puntos_x )
    puntos_y = map( float, puntos_y )

    puntos_filtrados_2 = list( filter( lambda v: m.get_pxl( *v ), zip( puntos_x, puntos_y ) ) )
    dic_indice_crimen = {}

    puntos_filtrados = []
    puntos_filtrados.extend( puntos_filtrados_1 )
    puntos_filtrados.extend( puntos_filtrados_2 )

    #matriz_distancias = scipy.spatial.distance.pdist( puntos_filtrados, metric='cityblock' )

    dist = sklearn.neighbors.DistanceMetric.get_metric('euclidean')
    matsim = dist.pairwise(puntos_filtrados)
    avSim = np.average(matsim)
    
    # 3. Building the Dendrogram	
    # http://docs.scipy.org/doc/scipy/reference/generated/scipy.cluster.hierarchy.linkage.html#scipy.cluster.hierarchy.linkage

    #puntos_filtrados = StandardScaler().fit_transform(puntos_filtrados)

    #labels = DBSCAN( metric='euclidean', eps = 0.0006, min_samples = 6 ).fit_predict( puntos_filtrados )
    labels = DBSCAN( metric='euclidean', eps = eps, min_samples = min_samples ).fit_predict( puntos_filtrados )

    # print( labels )

    # print( avSim )

    # clusters = cluster.hierarchy.linkage(matsim, method = 'ward')
    # cluster.hierarchy.dendrogram(clusters, color_threshold=0)
    # plt.show()
    
    # # 4. Cutting the dendrogram
    # #Forms flat clusters from the hierarchical clustering defined by the linkage matrix Z
    # clusters = cluster.hierarchy.fcluster(clusters, avSim, criterion = 'distance')
    # print clusters

    # print( len( matriz_distancias ) )


# Number of clusters in labels, ignoring noise if present.
    n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)

    print('Estimated number of clusters: %d' % n_clusters_)

##############################################################################
# Plot result

# Black removed and is used for noise instead.


    #plt.subplot( 111 )
    plt.figure(figsize=(12, 7), dpi=80)
    plt.imshow( m.get_map() )

    unique_labels = set(labels)

    media_elementos_cluster = 0
    min_elementos = None
    max_elementos = None
    s = list( sorted( labels ) )
    for t, it in groupby( s ):
        if t == -1: continue
        l = sum( 1 for _ in it )
        min_elementos = min( l, min_elementos or l )
        max_elementos = max( l, max_elementos or l )
        media_elementos_cluster += l

    media_elementos_cluster /= len( unique_labels ) - 1

    print( 'Media cluster: {}'.format( media_elementos_cluster ) )
    print( 'Min cluster: {}'.format( min_elementos ) )
    print( 'Max cluster: {}'.format( max_elementos ) )

    colors = []
    for i in range(len(unique_labels)):
        colors.append(generate_new_color(colors, pastel_factor = 0.5) )

    #colors = plt.cm.Spectral(np.linspace(0, 1, len(unique_labels)))
    for k, col in zip(unique_labels, colors):
        if k == -1:
            # Black used for noise.
            col = 'k'
            continue

        #(x, y), _ = zip( *( list( filter( lambda v: v[1] == k, zip( puntos_filtrados, labels ) ) ) ) )
        xy, _ = zip( *list( filter( lambda v: v[1] == k, zip( puntos_filtrados, labels ) ) ) )
        #x, y = zip( *xy )
        pxls = map( lambda v: m.get_pxl( *v ), xy )
        x, y = map( list, zip( *pxls ) )

        cx = sum( x ) / len( x )
        cy = sum( y ) / len( y )

        #plt.scatter( x, y, label = 'Cluster {}'.format( k ), color = col, s = 2 )
        plt.scatter( cx, cy, label = 'Cluster {}'.format( k ), color = col, s = len( x ) )

    plt.legend(loc=5, borderaxespad=-20, prop={'size':8})
    plt.title('Estimated number of clusters: %d' % n_clusters_)
    plt.show()






    # unique_labels = set(labels)
    # colors = plt.cm.Spectral(np.linspace(0, 1, len(unique_labels)))
    # for k, col in zip(unique_labels, colors):
        # if k == -1:
            # # Black used for noise.
            # col = 'k'

        # #(x, y), _ = zip( *( list( filter( lambda v: v[1] == k, zip( puntos_filtrados, labels ) ) ) ) )
        # xy, _ = list( zip( *list( filter( lambda v: v[1] == k, zip( puntos_filtrados, labels ) ) ) ) )
        # x, y = zip( *xy )

        # plt.plot(x, y, 'o', markerfacecolor=col,
                 # markeredgecolor='k', markersize=4)

    # plt.title('Estimated number of clusters: %d' % n_clusters_)
    # plt.show()

def imagenes_evolucion(d = '../Resultados/agrupacion_mes_densidad_0.00015_7/'):
	m = SFMap()

	datos_crimenes = get_data( './data/raw/Map__Crime_Incidents_-_from_1_Jan_2003.csv' )
	datos_incidentes = get_data( './data/raw/Case_Data_from_San_Francisco_311__SF311_.csv' )

	head_incidentes = set()
	for _,_,_,_,_,_,categoria_incidentes,_,_,_,_,_,_,_,_ in datos_incidentes:
		head_incidentes.add( categoria_incidentes )

	head_crimenes = set()
	for _,categoria_crimenes,_,_,_,_,_,_,_,_,_,_ in datos_crimenes:
		head_crimenes.add( categoria_crimenes )

	head_incidentes = list( sorted( head_incidentes ) )
	head_crimenes = list( sorted( head_crimenes ) )
	head_mod = head_crimenes + head_incidentes

	d = '../Resultados/agrupacion_mes_densidad_0.00015_7/'
	#d = '../Resultados/agrupacion_semana_densidad_0.00015_7/'
	ruta_guardado = "./imagenes/semana/"
	
	for fichero in sorted( listdir( d ) ):
		
		name = fichero[:fichero.find(".")]
		
		imagenes = listdir(ruta_guardado)		
		if(imagenes.count(name + ".png") != 0):
			continue
		
		plt.figure(figsize=(12, 7), dpi=80)
		plt.imshow( m.get_map() )

		if fichero[-4:] != '.csv': continue

		print( 'Analizando: {}'.format( fichero ) )

		tabla = get_data( d + fichero, titulo = True, fl = [float] )
		tabla_sin_titulo = list(tabla)[1:]
		tabla_filtrada = list( map( list, zip( * ( list( zip( *tabla_sin_titulo ) )[:3] ) ) ) )

		#tabla_estandarizada = list( estandariza_tabla( tabla_filtrada, head_mod ) )
		
		colors = []
		for i in range(len(tabla_filtrada)):
			colors.append(generate_new_color(colors, pastel_factor = 0.5) )

		#colors = plt.cm.Spectral(np.linspace(0, 1, len(unique_labels)))
		for k, col in zip(tabla_filtrada, colors):
			
			x, y = m.get_pxl(float(k[0]), float(k[1]))
			

			#plt.scatter( x, y, label = 'Cluster {}'.format( k ), color = col, s = 2 )
			plt.scatter( x, y, color = col, s = k[2] )
		
		
		plt.savefig(ruta_guardado + name)
		plt.close()




def prueba_fecha():
    eps = 0.0000001
    min_samples = 16

    y = 2009
    m = 1

    datos_crimenes = get_data( './data/raw/Map__Crime_Incidents_-_from_1_Jan_2003.csv' )
    datos_incidentes = get_data( './data/raw/Case_Data_from_San_Francisco_311__SF311_.csv' )

    datos_incidentes = limpiar_incidentes( datos_incidentes )

    dc = list( filtra_fecha_crimen( datos_crimenes, year = y, month = m ) )
    di = list( filtra_fecha_incidente( datos_incidentes, year = y, month = m ) )

    agrupa_puntos_crimenes_incidentes( dc, di, eps = eps, min_samples = min_samples )


def crea_tablas():
    eps = 0.00015
    min_samples = 7

    #for y in range( 2008, 2017 ):
    for y in range( 2012, 2017 ):
        for m in range( 0, 13 ):
            #try:
            datos_crimenes = get_data( './data/raw/Map__Crime_Incidents_-_from_1_Jan_2003.csv' )
            datos_incidentes = get_data( './data/raw/Case_Data_from_San_Francisco_311__SF311_.csv' )

            datos_incidentes = limpiar_incidentes( datos_incidentes )

            dc = list( filtra_fecha_crimen( datos_crimenes, year = y, month = m ) )
            di = list( filtra_fecha_incidente( datos_incidentes, year = y, month = m ) )

            if len( dc ) == 0 or len( di ) == 0: continue
            print( 'Analizando {} {}'.format( y, m ) )

            tabla = agrupa_puntos_crimenes_incidentes_tabla( dc, di, eps = eps, min_samples = min_samples )

            #dendograma_correlacion( list( tabla ) )

            dbfile = './resultados/agrupacion_mes_densidad_{}_{}/agrupacion_sucesos_{}_{}.csv'.format( eps, min_samples, y, m )
            if not os.path.exists( os.path.dirname( dbfile ) ):
                os.mkdir( os.path.dirname( dbfile ) )

            f = open( dbfile, 'wt')
            try:
                writer = csv.writer(f)
                for a,fila in tabla:
                    writer.writerow( list(a) + fila )
            finally:
                pass
            #except:
            #    pass

def crea_tablas_2():
    # datos_crimenes = get_data( './data/raw/Map__Crime_Incidents_-_from_1_Jan_2003.csv' )
    # datos_incidentes = get_data( './data/raw/Case_Data_from_San_Francisco_311__SF311_.csv' )
    
    # datos_incidentes = limpiar_incidentes( datos_incidentes )

    barrio = 'Mission'
    datos_crimenes = read_crimenes( barrio )
    datos_incidentes = read_incidentes( barrio )

    eps = 0.00015
    min_samples = 7

    f, f_tipo = agrupa_semana_incidente_crimen, 'semana'
    #f, f_tipo = agrupa_mes_incidente_crimen, 'mes'

    for ( year, numero_semana ), di, dc in f( datos_incidentes, datos_crimenes ):
        #try:

        if len( dc ) == 0 or len( di ) == 0: continue
        print( 'Analizando {} {}'.format( year, numero_semana ) )

        tabla = agrupa_puntos_crimenes_incidentes_tabla( dc, di, eps = eps, min_samples = min_samples )

        #dendograma_correlacion( list( tabla ) )

        dbfile = './resultados/agrupacion_{}_densidad_{}_{}/agrupacion_sucesos_{}_{:02}.csv'.format( f_tipo, eps, min_samples, year, numero_semana )
        if not os.path.exists( os.path.dirname( dbfile ) ):
            os.mkdir( os.path.dirname( dbfile ) )

        f = open( dbfile, 'wt')
        try:
            writer = csv.writer(f)
            for a,fila in tabla:
                writer.writerow( list(a) + fila )
        finally:
            pass
        #except:
        #    pass


imagenes_evolucion()

# for c in read_incidentes( 'Mission' ):
    # print( c )
# sys.exit( 0 )

# PRUEBA FECHA
#prueba_fecha()
#sys.exit( 0 )

# PARA CREAR LAS TABLAS DE LAS AGRUPACIONES
#crea_tablas()
#crea_tablas_2()
##sys.exit( 0 )

#datos_crimenes = get_data( './data/raw/Map__Crime_Incidents_-_from_1_Jan_2003.csv' )
#datos_incidentes = get_data( './data/raw/Case_Data_from_San_Francisco_311__SF311_.csv' )

#head_incidentes = set()
#for _,_,_,_,_,_,categoria_incidentes,_,_,_,_,_,_,_,_ in datos_incidentes:
    #head_incidentes.add( categoria_incidentes )

#head_crimenes = set()
#for _,categoria_crimenes,_,_,_,_,_,_,_,_,_,_ in datos_crimenes:
    #head_crimenes.add( categoria_crimenes )

#head_incidentes = list( sorted( head_incidentes ) )
#head_crimenes = list( sorted( head_crimenes ) )
#head_mod = head_crimenes + head_incidentes

##d = './resultados/agrupacion_mes_densidad_0.00015_7/'
#d = './resultados/agrupacion_semana_densidad_0.00015_7/'
#for fichero in sorted( listdir( d ) ):
    #if fichero[-4:] != '.csv': continue

    #print( 'Analizando: {}'.format( fichero ) )

    #tabla = get_data( d + fichero, titulo = True, fl = [float] )

    #tabla_filtrada = list( map( list, zip( * ( list( zip( *tabla ) )[3:] ) ) ) )

    #tabla_estandarizada = list( estandariza_tabla( tabla_filtrada, head_mod ) )

    ## POR SI SON LAS TABLAS ANTIGUAS
    ## fichero_tmp = fichero.split('_')
    ## fichero_mod = '_'.join( fichero_tmp[:-1] ) + '_'
    ## if len( fichero_tmp[ -1 ] ) == 5:
        ## fichero_mod += '0'
    ## fichero_mod += fichero_tmp[ -1 ][:-4]

    ## POR SI SON LAS NUEVAS
    #fichero_mod = fichero[:-4]

    #dendograma_correlacion( tabla_estandarizada, fichero_mod )

#sys.exit( 0 )


## GRAFICAR EL DENDOGRAMA DE LAS CORRELACIONES ENTRE CATEGORIAS
##fichero = './resultados/agrupacion_semana_densidad_0.00015_7/agrupacion_sucesos_2010_1.csv'
#fichero = './resultados/agrupacion_mes_densidad_0.00015_7/agrupacion_sucesos_2011_5.csv'
##fichero = './resultados/agrupacion_mes_densidad_0.00015_7/agrupacion_sucesos_2010_1.csv'
##fichero = './resultados/agrup_old/agrupacion_mes_densidad_0.0006_5/agrupacion_sucesos_2008_9.csv'

##tabla = get_data( fichero, titulo = True, fl = [int, float] )
#tabla = get_data( fichero, titulo = True, fl = [float] )

#tabla_filtrada = list( map( list, zip( * ( list( zip( *tabla ) )[3:] ) ) ) )

##print( tabla_filtrada )

#dendograma_correlacion( tabla_filtrada )
#sys.exit( 0 )


## DATOS NORMALES

#datos_crimenes = get_data( './data/raw/Map__Crime_Incidents_-_from_1_Jan_2003.csv' )
#datos_incidentes = get_data( './data/raw/Case_Data_from_San_Francisco_311__SF311_.csv' )


## DATOS PEQUEÑOS
##datos_crimenes = get_data( './data/little/Map__Crime_Incidents_-_from_1_Jan_2003.little.csv' )

##datos_incidentes = get_data( './data/little/Case_Data_from_San_Francisco_311__SF311_.little.csv' )

#datos_incidentes = limpiar_incidentes( datos_incidentes )

#y = 2014
#m = 3

#dc = list( filtra_fecha_crimen( datos_crimenes, year = y, month = m ) )
#di = list( filtra_fecha_incidente( datos_incidentes, year = y, month = m ) )

##agrupa_puntos_crimenes_incidentes( dc, di )
#tabla = agrupa_puntos_crimenes_incidentes_tabla( dc, di )

## cat = 'Litter Receptacles'
## grafica_agrupacion( list( tabla ), cat )

#dendograma_correlacion( list( tabla ) )

## f = open('./resultados/sucesos_{}_{}.csv'.format( y, m ), 'wt')
## try:
    ## writer = csv.writer(f)
    ## for fila in tabla:
        ## writer.writerow( fila )
## finally:
    ## f.close()
