#!/usr/bin/env python
# -*- coding: utf-8 -*-
import csv
import sqlite3 as lite
from datetime import datetime

from shapely.geometry import shape
from shapely.geometry import Point
import fiona
import utm

# function to read a csv file
def get_data( file_name, delimiter = ',', quotechar = '"', titulo=False, fl = [] ):
    data = None
    title = []
    with open( file_name ) as csvfile:
        reader = csv.reader(csvfile, delimiter=delimiter, quotechar=quotechar)
        for row in reader:
            if data == None:
                data = []
                for x in row:
                    title.append( x )
                if titulo:
                    yield tuple( title )
            else:
                l = []
                for x in row:
                    for f in fl + [ lambda x:x ]:
                        try:
                            l.append( f( x ) )
                            break
                        except:
                            pass
                yield tuple( l )

# Categorias tabla estandarizada
# categoria
# descripcion
# dia de la semana
# fecha
# hora
# distrito
# x
# y

def estandariza_tabla_crimenes( datos_crimenes ):
    for v in datos_crimenes:
        m, d, y = tuple( map( int, v[4].split( ' ' )[0].split( '/' ) ) )
        
        hora, minuto = tuple( map( int, v[5].split( ':' ) ) )

        fecha = datetime( y, m, d, hora, minuto )
        yield ( v[1].lower(), v[2].lower(), v[3].lower(), fecha, v[12].lower(), v[9], v[10] )

def loaddata(con, archive, insert):
	cursor= con.cursor()
	for fila in archive:
            cursor.execute(insert, fila)

	cursor.close()
	con.commit()

def make_sql_file_crimenes( data ):
    connection = lite.connect('./data/out/crimenes_preprocesados.db')

    with connection:
            connection.execute("CREATE TABLE Crimenes( Categoria TEXT, Descripcion TEXT, DiaSemana TEXT, Fecha TIMESTAMP, Distrito TEXT, X REAL, Y REAL )")
            connection.commit()

            loaddata(connection, data, 'INSERT INTO Crimenes VALUES (?,?,?,?,?,?,?)' )

def add_distrito( data ):
    array_neigh_shape = []
    with fiona.open('./data/in/sffind_neighborhoods/SFFind_Neighborhoods.shp', 'r') as source:
        for f in source:
            p = f['properties']
            g = f['geometry']

            name = p['name']
            sh = shape( g )

            array_neigh_shape.append( ( sh.area, ( name, sh ) ) )

    # we sort the values by area to obtain first the neighborshoods with less area
    _, array_neigh_shape = zip( *sorted( array_neigh_shape ) )
    array_neigh_shape = list( array_neigh_shape )

    for v in data:
        _,_,_,_,_,_,_,_,_,x,y,_ = v
        x = float( x )
        y = float( y )

        try:
            # check if the utm values exists 
            utm_x, utm_y, _, _ = utm.from_latlon(y, x)

            p = Point( x, y )

            for name, sh in array_neigh_shape:
                if sh.contains( p ):
                    yield tuple( v ) + ( name, )
                    break
        except:
            pass

# make a sql with the preprocessed data
t = get_data( './data/in/Map__Crime_Incidents_-_from_1_Jan_2003.csv' )
tl = add_distrito( t )
te = estandariza_tabla_crimenes( tl )
make_sql_file_crimenes( te )
